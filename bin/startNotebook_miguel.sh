#!/bin/bash

echo "In local machine: "
echo "ssh -N -f -L localhost:8889:localhost:8888 miguel@capuchino"
echo " "
echo "Open web browser at (copy token if necessary): "
echo "localhost:8889"

eval "$(/home/barri/miniconda3/condabin/conda shell.bash hook)"
/home/barri/miniconda3/condabin/conda activate mlam_cpu
cd /home/MLAM/bin
jupyter-lab --no-browser --port=8888

# Closing
#
# Remote: ctrl + c.
# Local:
#	sudo netstat -lpn | grep :8889
#	kill PIDs
