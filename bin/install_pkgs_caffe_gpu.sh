#!/bin/bash

# Create MLAM_gpu envirorment with all necessary packages.

TORCH=1.9.0 	# version de torch
CUDA=cu113  	# version de cuda

conda create -n mlam_gpu python=3.9
conda activate mlam_gpu

conda install -n mlam_gpu jupyterlab -c conda-forge
conda install -n mlam_gpu numpy matplotlib networkx tqdm pandas

conda install -n mlam_gpu pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch
conda install -n mlam_gpu pyg -c pyg -c conda-forge

conda install -n mlam_gpu jax -c conda-forge
conda install -n mlam_gpu -c coecms celluloid 