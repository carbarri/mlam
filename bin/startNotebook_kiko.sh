#!/bin/bash

# USAGE (you have to start your CONDA environment first).
# 
# 	bash startNotebook.sh <user> <port> <run_type>
# 
# <user> your user in capuchino
# <port> use only even ports starting in 8888. Check if port is being used with: sudo netstat -lpn | grep :<port>
# <run_type> = cpu or gpu

user=${1}
port=${2}
# runType=${3} 	# cpu or gpu
port2=$((${port}+1))

echo "In local machine: "
echo "ssh -N -f -L localhost:${port2}:localhost:${port} ${user}@147.96.71.59"
echo " "
echo "Open web browser at (copy token if necessary): "
echo "localhost:${port2}"

echo "For closing everything."
echo " "
echo "In remote machine: ctrl + c."
echo "In local machine:"
echo "   sudo netstat -lpn | grep :${port2}"
echo "   kill <PID from previous command>"

read -p "Press enter to continue."


session="jupyter"

# set up tmux
tmux start-server

# create a new tmux session
tmux new-session -d -s $session

# Select pane 0
tmux selectp -t 0

# Run htop
tmux send-keys "cd /home/MLAM/bin" C-m 
tmux send-keys "jupyter-lab --no-browser --port=${port}" C-m 

# Finished setup, attach to the tmux session!
tmux attach-session -t $session



# eval "$(conda shell.bash hook)"
# conda activate mlam_${runType}
# cd /home/MLAM/bin
# nohup jupyter-lab --no-browser --port=${port} &> jupyter-lab.log &
