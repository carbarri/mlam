import numpy as np
from IPython.display import clear_output
import glob, re

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in _nsre.split(s)] 

def importFiles(filename_glob_pattern, dtype='float', delimiter=None, skip_header=0, usecols=None):
    
    '''Import LAMMPS output dump files.
        
        Example: 
        
        frames = importFiles("/lammps/data/sim01/frames/ConRun.*", dtype=float, delimiter=" ", skip_header=9, usecols=range(0,8))'''
    
    sorted_file_list = sorted(glob.glob(filename_glob_pattern), key=natural_sort_key)
    data = []
    i=1;
    for file_path in sorted_file_list:
        data.append(
            np.genfromtxt(file_path, delimiter=delimiter, skip_header=skip_header, dtype=dtype, usecols=usecols))
        i+=1;
        if (i % 1000) == 0:
            clear_output(wait=True)
            print(i)
        
    print("Imported {} files.".format(len(data)))
        
    return data

def lammpsDump2numpy(frames):    
    data = np.asarray(frames);
    data_attr = np.array([[[np.concatenate((row.tolist()[2:6], np.array([0,1]))) for row in col] for col in data]]);
    data_accel = np.array([[[row.tolist()[6:8] for row in col] for col in data]]);
    
    return data_attr, data_accel