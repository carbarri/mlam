#!/bin/bash

# Create MLAM_cpu envirorment with all necessary packages.

TORCH=1.8.0
CUDA=cpu

conda create -n mlam_cpu python=3.7
conda install -n mlam_cpu jupyterlab -c conda-forge
conda install -n mlam_cpu pytorch cpuonly -c pytorch
conda activate mlam_cpu
conda install -n mlam_cpu pip
conda install -n mlam_cpu numpy matplotlib networkx tqdm pandas
conda install -n mlam_cpu jax -c conda-forge

pip install celluloid

pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
pip install torch-geometric
