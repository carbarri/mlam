#!/bin/bash

# Create MLAM_gpu envirorment with all necessary packages.

TORCH=1.9.0
CUDA=cu102

conda create -n mlam_gpu python=3.9
conda activate mlam_gpu

conda install -n mlam_gpu jupyterlab -c conda-forge
conda install -n mlam_gpu numpy matplotlib networkx tqdm pandas

conda install -n mlam_gpu pytorch cudatoolkit=10.2 -c pytorch -c conda-forge
# conda install -n mlam_gpu pytorch cudatoolkit=10.2 -c pytorch -c nvidia


# Instalar torch-geometric (nuevo)

conda install -n mlam_gpu pytorch-geometric -c rusty1s -c conda-forge

conda install -n mlam_gpu jax -c conda-forge

conda install -n mlam_gpu -c coecms celluloid

# # Instalar torch-geometric (antiguo)

# conda install -n mlam_gpu pip

# pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
# pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
# pip install torch-geometric

# pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html
# pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-${TORCH}+${CUDA}.html

# # conda install -n mlam_gpu jax -c conda-forge
# # pip install celluloid
